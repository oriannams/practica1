/*Copyright [2022] [Orianna Milone S.]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

package mates;
import java.lang.Math;
import java.util.Random;

/**
 * @author Orianna Milone
 */

public class Matematicas{

	 /**
	 * Metodo para generar una aproximacion al numero pi, a traves del metodo de Montecarlo.
	 * Toma como parametro la variable "pasos", que obtiene al ejecutar el programa desde consola.
	 * @param pasos
         * @return Aproximacion al numero pi (a traves del metodo de Montecarlo.)
	 */

	public static double generarNumeroPiIterativo(long pasos){
		double D = 0;
		for(int i = 0; i <= pasos; i++){
			double x = Math.random();
			double y = Math.random();
			double coordenada = (Math.pow(x,2)) + (Math.pow(y,2)); 
			if(coordenada <= 1){
				D += 1;
			}
		}
		return (4*D)/pasos;
	}
}
