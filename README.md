# Generador de aprox. al numero pi #
Metodo Montecarlo

![Licencia](licencias/by_petit.png)

# Autores:

> - **Orianna Milone S.**

## Descripcion 
El método de Montecarlo es un método de simulación que permite calcular estadísticamente el valor final de una secuencia de sucesos no deterministas (sujetos a variabilidad), como es el caso del plazo o el coste de un proyecto... Este conjunto de valores permite calcular el valor medio y la variabilidad para el conjunto.

	https://www.recursosenprojectmanagement.com/metodo-de-montecarlo/

## Menú de opciones:
  >*'make jar'* (Para generar el fichero ejecutable).

  >*'make compilar'* (Para compilar el programa).

  >*'java -jar ap-personas.jar "argumeto necesario"'*
## Ejemplo (previsualización)

- java -jar ap-personas.jar 600
> El número PI es 3.133 

### Cuenta con dos clases que permiten interactuar con la informacion:
> - Principal: Ejecutara el programa.
> - Matematicas: Contiene el algortimo donde se evoca al metodo de montecarlo, para generar (mediante un metodo iterativo) una aproximacion al numero pi.


## Licencias
Copyright [2022] [Orianna Milone]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

        Unless required by applicable law or agreed to in writing, software
        distributed under the License is distributed on an "AS IS" BASIS,
        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
        See the License for the specific language governing permissions and
        limitations under the License.


## Diagrama de programa
![Diagrama](DiagramUML.png)
